/* eslint no-return-assign: off */
/* eslint require-jsdoc: off */
/* eslint no-undef: off */
/* eslint no-console: off */

import { State, RenderingManager } from "@quotquot/element/dist/index.mjs";
import { TextAttrRenderer, InterpolateRenderer, ForRenderer, IfRenderer } from "../index.mjs";

beforeEach(() => {
    document.body.innerHTML = "";
});

test("Expr renderer", async() => {

    const state = new State({ x: 1, y: { z: 3 } });

    const renderer = new TextAttrRenderer();
    await renderer.setup(document.body, state);

    const manager = new RenderingManager();
    await manager.setup(document.body, state);
    manager.addRenderer(renderer);

    const template = document.createElement("template");
    template.innerHTML = '<span data-text="x" data-attr-z="y.z"></span>';
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe('<span z="3">1</span>');

    await state.update({ x: 2, y: { z: 4 } });
    expect(document.body.innerHTML).toBe('<span z="4">2</span>');

    manager.clear();
    await manager.cleanup(document.body, state);
    expect(document.body.innerHTML).toBe("");
});

test("Interpolate renderer", async() => {

    const state = new State({ x: 1, y: { z: 3 }, a: "a" });

    const renderer = new InterpolateRenderer();
    await renderer.setup(document.body, state);

    const manager = new RenderingManager();
    await manager.setup(document.body, state);
    manager.addRenderer(renderer);

    const template = document.createElement("template");
    template.innerHTML = '<template data-interpolate="">${x} ${y.z}<foo></foo>${a}</template>';
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe("<!--interpolate-->1 3<foo></foo>a");

    await state.update({ x: 2, y: { z: 4 } });
    expect(document.body.innerHTML).toBe("<!--interpolate-->2 4<foo></foo>a");

    template.innerHTML = '<template data-interpolate="">${a}<foo></foo>${y.z} ${x}!</template>';
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe("<!--interpolate-->a<foo></foo>4 2!");

    manager.clear();
    await manager.cleanup(document.body, state);
    expect(document.body.innerHTML).toBe("");

});

test("For renderer", async() => {

    const state = new State({ X: ["x1", "x2"], Y: ["y1", "y2"] });

    const forRenderer = new ForRenderer();
    await forRenderer.setup(document.body, state);

    const textAttrRenderer = new TextAttrRenderer();
    await textAttrRenderer.setup(document.body, state);

    const manager = new RenderingManager();
    await manager.setup(document.body, state);
    manager.addRenderer(forRenderer);
    manager.addRenderer(textAttrRenderer);

    const template = document.createElement("template");
    template.innerHTML = '<template data-for="x in X">111<foo bar="42"></foo></template><template data-else=""><bar></bar></template>';
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe('<!--for-->111<foo bar="42"></foo>111<foo bar="42"></foo>');

    template.innerHTML = '<template data-for="x,ix in X"><template data-for="y,iy in Y"><span data-attr-x="x" data-attr-ix="ix" data-attr-y="y" data-attr-iy="iy"></span></template></template><template data-else=""><bar></bar></template>';
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe('<!--for--><!--for--><span x="x1" ix="0" y="y1" iy="0"></span><span x="x1" ix="0" y="y2" iy="1"></span><!--for--><span x="x2" ix="1" y="y1" iy="0"></span><span x="x2" ix="1" y="y2" iy="1"></span>');

    await state.update({ X: ["x1", "x2", "x3"] });
    expect(document.body.innerHTML).toBe('<!--for--><!--for--><span x="x1" ix="0" y="y1" iy="0"></span><span x="x1" ix="0" y="y2" iy="1"></span><!--for--><span x="x2" ix="1" y="y1" iy="0"></span><span x="x2" ix="1" y="y2" iy="1"></span><!--for--><span x="x3" ix="2" y="y1" iy="0"></span><span x="x3" ix="2" y="y2" iy="1"></span>');

    await state.update({ X: [] });
    expect(document.body.innerHTML).toBe("<!--for--><bar></bar>");

    manager.clear();
    await manager.cleanup(document.body, state);
    expect(document.body.innerHTML).toBe("");
});

test("If renderer", async() => {

    const state = new State();

    const renderer = new IfRenderer();
    await renderer.setup(document.body, state);

    const manager = new RenderingManager();
    await manager.setup(document.body, state);
    manager.addRenderer(renderer);

    const x1 = "111";
    const x2 = "<span>222</span>";
    const x3 = '<foo bar="42"></foo>';
    const template = document.createElement("template");
    template.innerHTML = `<template data-if="x === 1">${x1}</template><template data-else-if="x === 2">${x2}</template><template data-else>${x3}</template>`;
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe("<!--if-->" + x3);

    await state.update({ x: 1 });
    expect(document.body.innerHTML).toBe("<!--if-->" + x1);

    await state.update({ x: 2 });
    expect(document.body.innerHTML).toBe("<!--if-->" + x2);

    await state.update({ x: 3 });
    expect(document.body.innerHTML).toBe("<!--if-->" + x3);

    template.innerHTML = template.innerHTML.replaceAll("x === 1", "x === 3");
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe("<!--if-->" + + x1);

    manager.clear();
    await manager.cleanup(document.body, state);
    expect(document.body.innerHTML).toBe("");
});
