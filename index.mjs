 /*
 * Copyright 2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import espr from "espr";
import logging from "logging";

import { AttrRenderer, BaseHandler, BaseTemplateHandler } from "@quotquot/element";

const logger = logging.getLogger("@quotquot/template");

/**
 * Initializes the template module. This is required and should be called
 * before any rendering is attempted.
 * @param {Function} _parse A function that takes a javascript expression as parameter
 *   and returns an ESPRIMA AST that reflects that expression.
 *        parse(expr: string): ASTnode
 * @param {Function} _evaluate A function that takes an AST and context as parameters,
 *        evaluates the former using the latter, and returns the result.
 *        evaluate(node: ASTnode, context: object): any
 * @param {Function} _variables A function that, given an AST, will return the name of
 *        variables that are referenced by it.
 * @param {Object} _logger A logging object with three methods: debug, warn, error.
 * @returns {Array} the array of rendering classes, to be added to quotquot elements
 */

class AttrSpec {

    #el;
    #value;
    #expression;
    #vars;
    #node;

    constructor(attr) {
        this.#el = attr.ownerElement;
        this.#value = attr.value;
        this.#expression = espr.getFromAttr(attr);
        this.#vars = espr.variables(this.#expression);
        this.#node = null;
        this.#el.removeAttributeNode(attr);
    }

    needsUpdate(changed) {
        return !changed.isDisjointFrom(this.#vars);
    }

    render(values, attrName) {
        const content = espr.evaluate(this.#expression, values);
        if (attrName) {
            this.#node = document.createAttribute(attrName);
            this.#node.value = content;
            logger.debug("setting", attrName, "to", content);
            this.#el.setAttributeNode(this.#node);
        }
        else { /* text */
            logger.debug("setting text to", content);
            this.#node = document.createTextNode(content);
            this.#el.appendChild(this.#node);
        }
    }

    clear() {
        if (this.#node) {
            if (this.#node.nodeType === Node.ATTRIBUTE_NODE)
                this.#el.removeAttributeNode(this.#node);
            else
                this.#el.removeChild(this.#node);
            this.#node = null;
        }
    }
}

class TextAttrHandler extends BaseHandler {

    static #ATTR_NAME = "data-text";
    static #ATTR_PREFIX = "data-attr-";

    static getAttributeNodes(el) {
        const attrs = [];
        for (const attr of el.attributes)
            if (attr.name === TextAttrHandler.#ATTR_NAME || attr.name.startsWith(TextAttrHandler.#ATTR_PREFIX))
                attrs.push(attr);
        return attrs.length ? attrs : null;
    }

    #textSpec;
    #attrSpecs;

    constructor(renderer, el, parent, attrs) {
        super(parent);
        this.#textSpec = null;
        this.#attrSpecs = new Map();
        if (attrs)
            for (const attr of attrs)
                this.#setAttr(attr);
    }

    #setAttr(attr) {
        const spec = new AttrSpec(attr);
        if (attr.name === TextAttrHandler.#ATTR_NAME)
            this.#textSpec = spec;
        else {
            const attrName = attr.name.slice(TextAttrHandler.#ATTR_PREFIX.length);
            this.#attrSpecs.set(attrName, spec);
        }
    }

    async render(manager, newState, context) {
        this.setContext(context);
        const values = this.getValues(newState);
        if (this.#textSpec)
            this.#textSpec.render(values);
        for (const [attrName, spec] of this.#attrSpecs.entries())
            spec.render(values, attrName);
    }

    async update(manager, oldState, newState, changed) {
        const values = this.getValues(newState);
        if (this.#textSpec && this.#textSpec.needsUpdate(changed)) {
            this.#textSpec.clear();
            this.#textSpec.render(values);
        }
        for (const [attrName, spec] of this.#attrSpecs.entries())
            if (spec.needsUpdate(changed)) {
                spec.clear();
                spec.render(values, attrName);
            }
    }

    clear() {
        if (this.#textSpec)
            this.#textSpec.clear();
        for (const spec of this.#attrSpecs.values())
            spec.clear();
    }
}

export class TextAttrRenderer extends AttrRenderer {
    constructor() {
        super(TextAttrHandler);
    }
}


export class InterpolateHandler extends BaseTemplateHandler {

    static DATA_INTERPOLATE = "data-interpolate";

    static getAttributeNodes(el) {
        if (el instanceof HTMLTemplateElement) {
            const attr = el.getAttributeNode(InterpolateHandler.DATA_INTERPOLATE);
            if (attr)
                return [attr];
        }
        return null;
    }

    #vars;
    #regexp;
    #groups;
    #content;
    #insertionNode;

    constructor(renderer, el, parent, attrs) {
        super(parent);
        this.#content = el.content;
        /* attrs is unused */
        this.#groups = [];
        this.#vars = new Set();
        this.#regexp = /\$\{([^{}]+)\}/g;
        for (const node of el.content.childNodes) {
            if (node.nodeType === Node.TEXT_NODE) {
                const groups = [];
                for (const m of node.nodeValue.matchAll(this.#regexp)) {
                    const ast = espr.getExpression(m[1]);
                    for (const name of espr.variables(ast))
                        this.#vars.add(name);
                    groups.push([m.index, m.index+m[0].length, ast]);
                }
                this.#groups.push(groups);
            }
        }
        this.#insertionNode = document.createComment("interpolate");
        el.before(this.#insertionNode);
        el.remove();
    }

    async #render(manager, newState) {
        const fragment = await this.instantiate(this.#content, manager, newState);
        const values = this.getValues(newState);
        let textIndex = 0;
        for (const node of fragment.childNodes) {
            if (node.nodeType === Node.TEXT_NODE) {
                const input = node.nodeValue;
                let output = "";
                let last = 0;
                for (const [start, end, ast] of this.#groups[textIndex]) {
                    output += input.slice(last, start) + espr.evaluate(ast, values);
                    last = end;
                }
                output += input.slice(last);
                node.nodeValue = output;
                textIndex++;
            }
        }
        this.#insertionNode.after(fragment);
    }

    async render(manager, newState, context) {
        this.setContext(context);
        await this.#render(manager, newState);
    }

    async update(manager, oldState, newState, changed) {
        const values = this.getValues(newState);
        if (!changed.isDisjointFrom(this.#vars)) {
            this.clear();
            await this.#render(manager, values);
        }
    }
}

export class InterpolateRenderer extends AttrRenderer {
    constructor() {
        super(InterpolateHandler);
    }
}


export class ForHandler extends BaseTemplateHandler {

    static DATA_FOR = "data-for";

    static getAttributeNodes(el) {
        if (el instanceof HTMLTemplateElement) {
            const attr = el.getAttributeNode(ForHandler.DATA_FOR);
            if (attr)
                return [attr];
        }
        return null;
    }

    #ast;
    #vars;
    #itemName;
    #indexName;
    #forContent;
    #elseContent;
    #selectedBranch;
    #insertionNode;

    constructor(renderer, el, parent, attrs) {
        super(parent);
        if (attrs)
            this.#setAttr(attrs[0]);
    }

    #setAttr(attr) {
        const forTemplate = attr.ownerElement;
        const sibling = forTemplate.nextElementSibling;

        this.#insertionNode = document.createComment("for");
        forTemplate.before(this.#insertionNode);

        this.#forContent = forTemplate.content;
        forTemplate.remove();

        if (sibling instanceof HTMLTemplateElement && sibling.hasAttribute("data-else")) {
            this.#elseContent = sibling.content;
            sibling.remove();
        }

        this.#itemName = "item";
        this.#indexName = "index";

        const expression = espr.getFromAttr(attr);

        // TODO: manage `i in range(start, end)` and `i=0; i<10; i++`
        if (expression.type === "Identifier" || expression.type === "MemberExpression") {
            // c
            // type: 'Identifier'
            // name: 'c'
            // a.b
            // type: 'MemberExpression'
            // computed: false
            // object:
            //   type: 'Identifier'
            //   name: 'a'
            // property:
            //   type: 'Identifier'
            //   name: 'b'
            this.#ast = expression;
        }
        else if (expression.type === "BinaryExpression" &&
                 expression.operator === "in" &&
                 expression.left.type === "Identifier") {
            // a in c
            // type: 'BinaryExpression'
            // operator: 'in'
            // left:
            //   type: 'Identifier'
            //   name: 'a'
            // right:
            //   type: 'Identifier'
            //   name: 'c'
            this.#itemName = expression.left.name;
            this.#ast = expression.right;
        }
        else if (expression.type === "SequenceExpression" &&
                 expression.expressions[0].type === "Identifier" &&
                 expression.expressions[1].type === "BinaryExpression" &&
                 expression.expressions[1].operator === "in" &&
                 expression.expressions[1].left.type === "Identifier") {
        /*else if (expression.type === "Compound" &&
                 expression.body[0].type === "Identifier" &&
                 expression.body[1].type === "BinaryExpression" &&
                 expression.body[1].operator === "in" &&
                 expression.body[1].left.type === "Identifier") {*/
            // a,b in c
            // type: 'Compound'
            // body:
            //   0:
            //     type: 'Identifier'
            //     name: 'a'
            //   1:
            //     type: 'BinaryExpression'
            //     operator: 'in'
            //     left:
            //       type: 'Identifier'
            //       name: 'b'
            //     right:
            //       type: 'Identifier'
            //       name: 'c'
            this.#itemName = expression.expressions[0].name;
            this.#indexName = expression.expressions[1].left.name;
            this.#ast = expression.expressions[1].right;
        }
        else
            throw new Error(`invalid for expression "${attr.value}"`);

        this.#vars = espr.variables(this.#ast);
    }

    async #render(manager, newState) {
        const values = this.getValues(newState);
        const result = espr.evaluate(this.#ast, values);
        if (!Array.isArray(result))
            throw new Error(`for expression is not an array: ${result}`);

        if (result.length) {
            const fragment = new DocumentFragment();
            const forloop = {}; /* like django */
            const newContext = { forloop };
            for (let index = 0; index < result.length; ++index) {
                newContext[this.#itemName] = result[index];
                newContext[this.#indexName] = index;
                forloop.first = index === 0;
                forloop.last = index === result.length - 1;
                fragment.append(await this.instantiate(this.#forContent, manager, newState, newContext));
            }
            logger.debug("appending", fragment.childNodes.length, "nodes");
            this.#insertionNode.after(fragment);
            this.#selectedBranch = this.el;
        }
        else if (this.#elseContent) {
            const fragment = await this.instantiate(this.#elseContent, manager, newState);
            this.#insertionNode.after(fragment);
            this.#selectedBranch = this.#elseContent;
        }
    }

    async render(manager, newState, context) {
        this.setContext(context);
        await this.#render(manager, newState);
    }

    async update(manager, oldState, newState, changed) {
        const values = this.getValues(newState);
        if (!changed.isDisjointFrom(this.#vars)) {
            this.clear();
            await this.#render(manager, values);
        }
    }
}

export class ForRenderer extends AttrRenderer {
    constructor() {
        super(ForHandler);
    }
}


class IfHandler extends BaseTemplateHandler {

    static DATA_IF = "data-if";
    static DATA_ELSE_IF = "data-else-if";
    static DATA_ELSE = "data-else";

    static getAttributeNodes(el) {
        if (el instanceof HTMLTemplateElement) {
            const attr = el.getAttributeNode(IfHandler.DATA_IF);
            if (attr)
                return [attr];
        }
        return null;
    }

    #branches;
    #elseContent;
    #selectedBranch;
    #insertionNode;

    constructor(renderer, el, parent, attrs) {
        super(parent);
        this.#branches = [];
        if (attrs)
            this.#setAttr(attrs[0]);
    }

    #setAttr(attr) {
        const ifTemplate = attr.ownerElement;
        logger.debug("if-renderer: initializing handler for", ifTemplate.outerHTML);

        this.#insertionNode = document.createComment("if");
        ifTemplate.before(this.#insertionNode);

        /* attr is data-if; we add it as a branch and check the next template */
        let template = this.#addBranch(ifTemplate, attr);

        while (template) {

            /* check if the current template is data-else-if */
            attr = template.getAttributeNode(IfHandler.DATA_ELSE_IF);

            if (attr) {
                /* it is; add it as a branch and continue */
                logger.debug("if-renderer: else-if branch:", template.outerHTML);
                template = this.#addBranch(template, attr);
            }
            else {

                /* if the template is data-else, remember it */
                if (template.getAttributeNode(IfHandler.DATA_ELSE)) {
                    logger.debug("if-renderer: else branch:", template.outerHTML);
                    this.#elseContent = template.content;
                    template.remove();
                }

                /* in all cases, we reached the end of the if-else block */
                break;
            }
        }
    }

    #addBranch(template, attr) {
        logger.debug("if-renderer: adding branch:", attr.name, attr.value);
        const expression = espr.getFromAttr(attr);
        this.#branches.push([ template.content, expression, espr.variables(expression) ]);
        const sibling = template.nextElementSibling;
        template.remove();
        return sibling instanceof HTMLTemplateElement ? sibling : null;
    }

    async #render(manager, newState) {
        const values = this.getValues(newState);
        for (const branch of this.#branches) {
            const [content, ast, vars] = branch;
            if (espr.evaluate(ast, values)) {
                const fragment = await this.instantiate(content, manager, newState);
                this.#insertionNode.after(fragment);
                this.#selectedBranch = branch;
                return;
            }
        }
        if (this.#elseContent) {
            const fragment = await this.instantiate(this.#elseContent, manager, newState);
            this.#insertionNode.after(fragment);
            this.#selectedBranch = this.#elseContent;
        }
        else
            this.#selectedBranch = null;
    }

    async render(manager, newState, context) {
        this.setContext(context);
        await this.#render(manager, newState);
    }

    async update(manager, oldState, newState, changed) {
        const values = this.getValues(newState);
        for (const [template, ast, vars] of this.#branches) {
            if (!changed.isDisjointFrom(vars)) {
                this.clear();
                await this.#render(manager, values);
            }
            // do not evaluate branches after the currenly selected one
            if (template === this.#selectedBranch)
                break;
        }
    }
}

export class IfRenderer extends AttrRenderer {
    constructor() {
        super(IfHandler);
    }
}

export default [TextAttrRenderer, InterpolateRenderer, ForRenderer, IfRenderer];
