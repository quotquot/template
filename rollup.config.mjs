import { terser } from "rollup-plugin-terser";

export default {
    input: "index.mjs",
    output: {
        file: "dist/index.mjs",
        format: "esm"
    },
    external: ["espr", "logging", "@quotquot/element"],
    plugins: [
        process.env.BUILD === "production" && terser()
    ]
};
